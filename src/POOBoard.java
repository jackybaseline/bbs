package bbs;
import java.io.Serializable;
public class POOBoard implements Serializable
{
	private String name;
	private POOArticle[] article;
	private int article_count;
	/**create a board with the name*/
	public POOBoard(String name)
	{
    	this.name=name;
		article=new POOArticle[0];
  	}
	/**check the board existing in the favorite or not */
	public boolean checkrepeat(String name)
	{
		if(this.name.compareTo(name)==0)
			return true;
		return false;
	}
  	/**append the article to the board*/
  	public void add(POOArticle article)
  	{
		POOArticle[] temp=this.article;
		this.article=new POOArticle[++article_count];
		for(int i=0;i<article_count-1;++i)
			this.article[i]=temp[i];
		this.article[article_count-1]=article;
  	}
  	/**delete the article at position pos*/
  	public void del(int pos,User user)
  	{
		if(pos<=0||pos-1>article_count)
		{
			System.out.println("Invalid position. ");
			return;
		}
		if(user.isroot()||user.getaccount().compareTo(article[pos-1].getauthor())==0)
		{
			for(int i=pos-1;i<article_count-1;++i)
				article[i]=article[i+1];
			POOArticle temp[]=article;
			article=new POOArticle[--article_count];
			for(int i=0;i<article_count;++i)
				article[i]=temp[i];
		}
		else
			System.out.println("Invalid action because you are not the superuser or the author of this article.");
  	}
  	/**move the article at position src to position dest*/
  	public void move(int src, int dest)
  	{
		if(src<=0||src-1>article_count)
		{
			System.out.println("Invalid source position. ");
			return;
		}
		else if(dest<=0||dest-1>article_count)
		{
			System.out.println("Invalid destination position. ");
			return;
		}
		else if(src==dest)
		{
			System.out.println("The source position is the same as destination position. ");
			return;
		}
		POOArticle temp=article[dest-1];
		article[dest-1]=article[src-1];
		article[src-1]=temp;
  	}
  	/**get the current number of articles in the board*/
  	public int getlength()
  	{
   		return article_count;
  	}
  	//show the article titles of the board
  	public void show()
  	{
		System.out.println("------------------------------");
		System.out.println("Now board: "+name);
		System.out.printf("Position  ID   hot   Author     title\n");
		if(article.length==0)
			System.out.println("    Empty. \n");
		for(int i=0;i<article_count;++i)
		{
			System.out.printf("%5d  ",(i+1));
			article[i].list();
		}
	}
	public POOArticle gointo(int pos)
	{
		if(pos<=0||pos-1>article_count)
		{
			System.out.println("Invalid position. ");
			return null;
		}
		return article[pos-1];
	}
	public String getname()
	{
		return name;
	}
	public int getarticle_count()
	{
		return article_count;
	}
	public int searchid(int search_id)
	{
		for(int i=0;i<article_count;++i)
			if(article[i].getid()==search_id)
				return i+1;
		System.out.println("Can't find ID "+search_id);
		return -1;
	}
}