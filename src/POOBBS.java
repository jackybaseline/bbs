package bbs;
import java.util.Scanner;
import java.io.*;
import java.io.Serializable;
public class POOBBS
{
	public static void main(String[] args)
	{
		POODirectory homepage=new POODirectory("Homepage");
		Scanner scanner=new Scanner(System.in);
		int board_count=0,thisuser=-1,trypassword_count=5;
		String account,password,decision;
		User[] user=new User[0];
		System.out.println("Welcome to BBS!");
		System.out.println("Please enter your: ");
		System.out.print("Account: ");
		account=scanner.next();
		System.out.print("Password: ");
		password=scanner.next();
		try
		{
			FileInputStream fis = new FileInputStream("secret");
			ObjectInputStream in = new ObjectInputStream(fis);
			user=(User[])in.readObject();
			in.close();
			fis.close();
		}
		catch  (Exception e)
		{
            System.out.println("Congratulations! You are the first user use this BBS. ");
        }
		try
		{
			FileInputStream fis1 = new FileInputStream("data");
			ObjectInputStream in1 = new ObjectInputStream(fis1);
			homepage=(POODirectory)in1.readObject();
			in1.close();
			fis1.close();
		}
		catch  (Exception e)
		{
            System.out.println("BBS. Start to work! ");
        }
		for(int i=0;i<user.length;++i)
		{
			switch(user[i].check(account,password))
			{
			case 1:
				thisuser=i;
				break;
			case -1:
				--trypassword_count;
				if(trypassword_count==0)
				{
					System.out.println("Try 5 times and all passwords are wrong. Are you a hacker?");
					return;
				}
				System.out.print("Please enter your password again (you still have "+trypassword_count+" chances): ");
				password=scanner.next();
				i=-1;
				break;
			}
		}
		if(thisuser==-1)
		{	
			if(user.length==0)
			{
				user=new User[1];
				user[0]=new User(account,password);
			}
			else
			{
				User temp[]=user;
				user=new User[user.length+1];
				for(int i=0;i<user.length-1;i++)
					user[i]=temp[i];
				user[user.length-1]=new User(account,password);
			}
			System.out.println("Welcome to BBS, new user. ");
			thisuser=user.length-1;
		}
		user[thisuser].checkroot();
		System.out.println("It's the "+user[thisuser].getlogin_count()+"th time you log in.");
		if(user[thisuser].isroot())
			System.out.println("You are superuser! You can add new board or delete existing board in the BBS. ");
		
		while(true)
		{
			System.out.println("------------------------------");
			System.out.println("(h) Homepage");
			System.out.println("(f) Favorite");
			System.out.println("(g) Goodbye");
			System.out.println("Please enter your decision(h,f,g): ");
			decision=scanner.next();
			if(decision.length()>1)
			{
				System.out.println("Invalid decision. ");
				continue;
			}
			switch(decision.charAt(0))
			{
				case 'h':
					gointo(homepage,user[thisuser]);
					break;
				case 'f':
					gointofavorite(user[thisuser].getfavorite(),user[thisuser]);
					break;
				case 'g':
					System.out.println("Goodbye! Thanks for using. ");
					break;
				default:
					System.out.println("Invalid decision. ");
			}
			if(decision.compareTo("g")==0)
				break;	
		}		
		try
		{
			FileOutputStream fos = new FileOutputStream("secret");
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(user);
			fos.close();
			out.close();  
		}
		catch(IOException e)
		{
            System.out.println(e.toString());
        }
		try
		{
			FileOutputStream fos1 = new FileOutputStream("data");
			ObjectOutputStream out1 = new ObjectOutputStream(fos1);
			out1.writeObject(homepage);
			fos1.close();
			out1.close();  
		}
		catch(IOException e)
		{
            System.out.println(e.toString());
        }
	}
	private static void gointo(POODirectory now,User user)
	{
		Scanner scanner=new Scanner(System.in);
		String action,name;
		int num,num1;
		while(true)
		{
			now.show();
			System.out.println("Please enter your action: (f) add to favorite (i) go into board or directory above (g) go back");
			System.out.println("If you are superuser, you can: (a) add directory (b) add board (d) delete (m) move (s) add split ");
			action=scanner.next();
			if(action.length()>1)
			{
				System.out.println("Invalid decision. ");
				continue;
			}
			switch(action.charAt(0))
			{
				case 'a':
					if(!user.isroot())
					{
						System.out.println("Invalid action because you are not the superuser.");
						continue;
					}
					System.out.print("Please enter the directory's name which you want to add: ");
					name=scanner.next();
					POODirectory newdirectory=new POODirectory(name);
					now.add(newdirectory);
					break;
				case 'b':
					if(!user.isroot())
					{
						System.out.println("Invalid action because you are not the superuser.");
						continue;
					}
					System.out.print("Please enter the board's name which you want to add: ");
					name=scanner.next();
					POOBoard newboard=new POOBoard(name);
					now.add(newboard);
					break;
				case 's':
					if(!user.isroot())
					{
						System.out.println("Invalid action because you are not the superuser.");
						continue;
					}
					now.add_split();
					break;
				case 'd':
					if(!user.isroot())
					{
						System.out.println("Invalid action because you are not the superuser.");
						continue;
					}
					System.out.print("Please enter the position which you want to delete: ");
					num=scanner.nextInt();
					now.del(num);
					break;
				case 'm':
					if(!user.isroot())
					{
						System.out.println("Invalid action because you are not the superuser.");
						continue;
					}
					System.out.print("Please enter the position of source: ");
					num=scanner.nextInt();
					System.out.print("Please enter the position of destination: ");
					num1=scanner.nextInt();
					now.move(num,num1);
					break;
				case 'f':
					System.out.print("Please enter the position of board which you want to add to favorite: ");
					num=scanner.nextInt();
					if(num<=0||num-1>now.getlength())
						System.out.println("Invalid position. ");
					else if(now.checkinstance(num)<=0)
						System.out.println("It is not a board so it can't be added to your favorite.");
					else
						user.addfavorite(now.getboard(num));
					break;
				case 'i':
					System.out.print("Please enter the position of board or directory which you want to go into: ");
					num=scanner.nextInt();
					if(num<=0||num-1>now.getlength())
						System.out.println("Invalid position. ");
					else if(now.checkinstance(num)>0)
						gointo(now.getboard(num),user);
					else if(now.checkinstance(num)<0)
						gointo(now.getdirectory(num),user);
					break;
				case 'g':
					break;
				default:
					System.out.print("Invalid action.");
			}
			if(action.compareTo("g")==0)
				return ;	
		}
	}
	private static void gointo(POOBoard now,User user)
	{
		Scanner scanner=new Scanner(System.in);
		String action,title,content,str;
		int num,num1;
		while(true)
		{
			now.show();
			System.out.println("Please enter your action: (a) add article (i) go into the article above (g) go back (#)search by ID");
			System.out.println("If you are superuser, you can: (d) delete article (or you are that author) (m) move");
			action=scanner.next();
			if(action.length()>1)
			{
				System.out.println("Invalid decision. ");
				continue;
			}
			switch(action.charAt(0))
			{
				case 'a':
					System.out.print("Please enter your article's title: ");
					scanner=new Scanner(System.in);
					title=scanner.nextLine();
					System.out.println("Please enter your article's content: ");
					System.out.println("If you finish, enter (s)save in the last line;(a)abandon for abandoning your input. ");
					content="";
					str="initial";
					while(true)
					{
						scanner=new Scanner(System.in);
						str=scanner.nextLine();
						if(str.compareTo("s")==0||str.compareTo("a")==0)
							break;
						content+=str;
						content+="\n";
					}
					if(str.compareTo("s")==0)	
					{
						POOArticle newarticle=new POOArticle(title,user.getaccount(),content,now.getarticle_count());
						now.add(newarticle);
					}
					break;
				case 'd':
					System.out.print("Please enter the position which you want to delete: ");
					num=scanner.nextInt();
					now.del(num,user);
					break;
				case 'm':
					if(!user.isroot())
					{
						System.out.println("Invalid action because you are not the superuser.");
						continue;
					}
					System.out.print("Please enter the position of source: ");
					num=scanner.nextInt();
					System.out.print("Please enter the position of destination: ");
					num1=scanner.nextInt();
					now.move(num,num1);
					break;
				case 'i':
					System.out.print("Please enter the position of article which you want to go into: ");
					num=scanner.nextInt();
					if(now.gointo(num)==null);
					else
						gointo(now.gointo(num),user);
					break;
				case 'g':
					break;
				case '#':
					System.out.print("Please enter ID of the article: ");
					num=scanner.nextInt();
					num=now.searchid(num);
					if(num!=-1)
						gointo(now.gointo(num),user);
					
				default:
					System.out.print("Invalid action.");
			}
			if(action.compareTo("g")==0)
				return ;	
		}
	}
	private static void gointo(POOArticle now,User user)
	{
		Scanner scanner=new Scanner(System.in);
		String action,comment,str="initial";
		while(true)
		{
			now.show();
			System.out.println("Please enter your action: (p) push  (b) boo (a) arrow (g) go back");
			System.out.println("If you are this author, you can: (r) revise content (t) revise title");
			action=scanner.next();
			if(action.length()>1)
			{
				System.out.println("Invalid decision. ");
				continue;
			}
			switch(action.charAt(0))
			{
				case 'p':
					System.out.print("Please enter your comment: ");
					scanner=new Scanner(System.in);
					comment=scanner.nextLine();
					now.push(comment,user.getaccount());
					break;
				case 'b':
					System.out.print("Please enter your comment: ");
					scanner=new Scanner(System.in);
					comment=scanner.nextLine();
					now.boo(comment,user.getaccount());
					break;
				case 'a':
					System.out.print("Please enter your comment: ");
					scanner=new Scanner(System.in);
					comment=scanner.nextLine();
					now.arrow(comment,user.getaccount());
					break;
				case 'r':
					if(user.getaccount().compareTo(now.getauthor())!=0)
						System.out.print("You are not the author!");
					else
					{	
						System.out.println("Please enter the content which you want to revise into: ");
						System.out.println("If you finish, enter (s)save in the last line;(a)abandon for abandoning your input. ");
						comment="";
						str="initial";
						while(true)
						{
							scanner=new Scanner(System.in);
							str=scanner.nextLine();
							if(str.compareTo("s")==0||str.compareTo("a")==0)
								break;
							comment+=str;
							comment+="\n";
						}
						if(str.compareTo("s")==0)	
							now.revisecontent(comment);
					}
					break;
				case 't':
					if(user.getaccount().compareTo(now.getauthor())!=0)
						System.out.print("You are not the author!");
					else
					{	
						System.out.print("Please enter the title which you want to revise into: ");
						scanner=new Scanner(System.in);
						comment=scanner.nextLine();
						now.revisetitle(comment);
					}
					break;			
				case 'g':
					break;
				default:
					System.out.print("Invalid action.");
			}
			if(action.compareTo("g")==0)
				return ;
		}
	}
	private static void gointofavorite(POODirectory now,User user)
	{
		Scanner scanner=new Scanner(System.in);
		String action,name;
		int num,num1;
		while(true)
		{
			now.show();
			System.out.println("Please enter your action: (s) add split (d) delete (m) move ");
			System.out.println("(i) go into board above (g) go back");
			action=scanner.next();
			if(action.length()>1)
			{
				System.out.println("Invalid decision. ");
				continue;
			}
			switch(action.charAt(0))
			{
				case 's':
					now.add_split();
					break;
				case 'd':
					System.out.print("Please enter the position which you want to delete: ");
					num=scanner.nextInt();
					now.del(num);
					break;
				case 'm':
					System.out.print("Please enter the position of source: ");
					num=scanner.nextInt();
					System.out.print("Please enter the position of destination: ");
					num1=scanner.nextInt();
					now.move(num,num1);
					break;
				case 'i':
					System.out.print("Please enter the position of board which you want to go into: ");
					num=scanner.nextInt();
					if(num<=0||num-1>now.getlength())
						System.out.println("Invalid position. ");
					else if(now.checkinstance(num)>0)
						gointo(now.getboard(num),user);
					break;
				case 'g':
					break;
				default:
					System.out.print("Invalid action.");
			}
			if(action.compareTo("g")==0)
				return ;	
		}
	}
}
class User implements Serializable
{ 
	private String account;
	private String password;
	private boolean root;
	private POODirectory favorite;
	private int login_count;
	public User(String account,String password)
	{
		this.account=account;
		this.password=password;
		favorite=new POODirectory(account+"'s favorite");
	}
	public int check(String account,String password)
	{
		if(this.account.compareTo(account)!=0)
			return 0;
		else if(this.password.compareTo(password)!=0)
		{	
			System.out.println("password error");
			return -1;
		}
		return 1;
	}	
	public String getaccount()
	{
		return account;
	}
	public void checkroot()
	{
		++login_count;
		if(account.compareTo("b00902062")==0||account.compareTo("iloveoop")==0)
			root=true;			
	}
	public boolean isroot()
	{
		if(root)
			return true;
		return false;
	}
	public POODirectory getfavorite()
	{
		return favorite;
	}
	public void addfavorite(POOBoard board)
	{
		for(int i=0;i<favorite.getlength();++i)
		{
			if(favorite.checkinstance(i+1)>0)
			{
				if(favorite.getboard(i+1).checkrepeat(board.getname()))
				{
					System.out.println("This board had already added to your favorite. ");
					return ;
				}
			}
		}
		favorite.add(board);
	}
	public int getlogin_count()
	{
		return login_count;
	}
}

