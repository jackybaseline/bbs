package bbs;
import java.io.Serializable;
public class POOArticle implements Serializable
{
	private int id;
	private String title;
	private String author;
	private String content;
	private int evaluation_count;
	private String[] evaluation_messages;
	POOArticle(String title,String author,String content,int article_count)
	{
		this.title=title;
		this.author=author;
		this.content=content;
		id=article_count+1;
		evaluation_messages=new String[0];
	}
	public String getauthor()
	{
		return author;
	}
	public void revisecontent(String content)
	{
		this.content=content;
	}
	public void revisetitle(String title)
	{
		this.title=title;
	}
	public void push(String str,String account)
	{
		String[] temp=evaluation_messages;
		++evaluation_count;
		evaluation_messages=new String[evaluation_messages.length+1];
		for(int i=0;i<evaluation_messages.length-1;++i)
			evaluation_messages[i]=temp[i];
		evaluation_messages[evaluation_messages.length-1]="Push "+account+" : "+str;
	}
	public void boo(String str,String account)
	{
		String[] temp=evaluation_messages;
		--evaluation_count;
		evaluation_messages=new String[evaluation_messages.length+1];
		for(int i=0;i<evaluation_messages.length-1;++i)
			evaluation_messages[i]=temp[i];
		evaluation_messages[evaluation_messages.length-1]="Boo  "+account+" : "+str;
	}
	public void arrow(String str,String account)
	{
		String[] temp=evaluation_messages;
		evaluation_messages=new String[evaluation_messages.length+1];
		for(int i=0;i<evaluation_messages.length-1;++i)
			evaluation_messages[i]=temp[i];
		evaluation_messages[evaluation_messages.length-1]="-->  "+account+" : "+str;
	}
	/**shows all the article information, the content, and the evaluation messages*/
	public void show()
	{
		System.out.println("------------------------------");
		System.out.println("ID: "+id+"\n");
		System.out.println("Title: "+title+"\n");
		System.out.println("Author: "+author+"\n");
		System.out.printf("Content: \n\n%s\n",content);
		System.out.println("------------------------------");
		for(int i=0;i<evaluation_messages.length;i++)
			System.out.println(evaluation_messages[i]);
	}
	/**shows only the evaluation count, the ID, the title, and the author*/
	public void list()
	{
		if(evaluation_count>=100)
			System.out.printf("%5d   Bomb %10s  %s\n",id,author,title);
		else if(evaluation_count<-10&&evaluation_count>-100)
			System.out.printf("%5d   X%d   %10s  %s\n",id,(-evaluation_count)/10,author,title);
		else if(evaluation_count<0&&evaluation_count>-10)
			System.out.printf("%5d      %10s  %s\n",id,(-evaluation_count)/10,author,title);
		else if(evaluation_count<=-100)
			System.out.printf("%5d   XX   %10s  %s\n",id,author,title);
		else
			System.out.printf("%5d   %2d   %10s  %s\n",id,evaluation_count,author,title);		
	} 
	public int getid()
	{
		return id;
	}
}