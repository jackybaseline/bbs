package bbs;
import java.io.Serializable;
public class POODirectory implements Serializable
{
	private int length;
	private int[] index;
	private POODirectory[] directory;
	private POOBoard[] board;
	private String name;
	public POODirectory(String name)
	{
		this.name=name;
		directory=new POODirectory[0];
		board=new POOBoard[0];
		POODirectory[] directory;
  	}
/**append the board to the directory*/
	public void add(POOBoard newboard)
	{
		POOBoard[] temp=board;
		board=new POOBoard[board.length+1];
		for(int i=0;i<board.length-1;++i)
			board[i]=temp[i];
		board[board.length-1]=newboard;
		int[] temp1=index;
		index=new int[++length];
		for(int i=0;i<length-1;++i)
			index[i]=temp1[i];
		index[length-1]=board.length;
	}
/**append the directory to the directory*/
	public void add(POODirectory newdirectory)
	{
		POODirectory[] temp=directory;
		directory=new POODirectory[directory.length+1];
		for(int i=0;i<directory.length-1;++i)
			directory[i]=temp[i];
		directory[directory.length-1]=newdirectory;
		int[] temp1=index;
		index=new int[++length];
		for(int i=0;i<length-1;++i)
			index[i]=temp1[i];
		index[length-1]=-(directory.length);
	}
/**append a splitting line to the directory*/
  	public void add_split()
  	{
		int[] temp1=index;
		index=new int[++length];
		for(int i=0;i<length-1;++i)
			index[i]=temp1[i];
		index[length-1]=0;
  	}
	 /**delete the board/directory/splitting line at position pos*/
	public void del(int pos)
	{
		if(pos<=0||pos-1>length)
		{
			System.out.println("Invalid position. ");
			return;
		}
		if(index[pos-1]>0)              
		{
			for(int i=(index[pos-1]-1);i<board.length-1;++i)
				board[i]=board[i+1];
			POOBoard[] temp=board;
			board=new POOBoard[board.length-1];
			for(int i=0;i<board.length;++i)
				board[i]=temp[i];
		}
		else if(index[pos-1]<0)
		{
			for(int i=-(index[pos-1]+1);i<directory.length-1;++i)
				directory[i]=directory[i+1];
			POODirectory[] temp=directory;
			directory=new POODirectory[directory.length-1];
			for(int i=0;i<directory.length;++i)
				directory[i]=temp[i];
		}
		for(int i=pos-1;i<length-1;++i)
			index[i]=index[i+1];
		int[] temp1=index;
		index=new int[--length];
		for(int i=0;i<length;++i)
			index[i]=temp1[i];
	}
	/**move the board/directory/splitting line at position src to position dest*/
  	public void move(int src, int dest)
  	{
		if(src<=0||src-1>length)
		{
			System.out.println("Invalid source position. ");
			return;
		}
		else if(dest<=0||dest-1>length)
		{
			System.out.println("Invalid destination position. ");
			return;
		}
		else if(src==dest)
		{
			System.out.println("The position of source is the same as position of destination. ");
			return;
		}
		int temp=index[src-1];
		index[src-1]=index[dest-1];
		index[dest-1]=temp;
  	}
	/**get the current number of items in the directory   */
  	public int getlength()
  	{
		return length;
  	}
	/**show the board/directory titles and splitting lines of the directory*/
  	public void show()
	{
		System.out.println("------------------------------");
		System.out.println("Now directory: "+name);
		System.out.println("Position             name");
		if(length==0)
			System.out.println("    Empty. \n");
		for(int i=0;i<length;i++)
		{
			System.out.printf("%5d.    ",(i+1));
			if(index[i]>0)
				System.out.println("board:     "+board[index[i]-1].getname());
			else if(index[i]<0)
				System.out.println("directory: "+directory[-(index[i]+1)].getname());
			else
				System.out.println("-------    ----------------------------------");
		}
  	}
	public int checkinstance(int pos)
	{
		if(index[pos-1]>0)
			return 1;
		else if(index[pos-1]<0)
			return -1;
		else
			return 0;
	}
	public POOBoard getboard(int pos)
	{
		return board[index[pos-1]-1];
	}
	public POODirectory getdirectory(int pos)
	{
		return directory[-(index[pos-1]+1)];
	}
	public String getname()
	{
		return name;
	}
}