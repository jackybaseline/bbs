main:
	javac -d . src/POOArticle.java
	javac -d . src/POOBoard.java
	javac -d . src/POODirectory.java
	javac -d . src/POOBBS.java
run:
	java src/POOBBS
clean:
	rm src/secret -f
	rm src/data -f
